/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODMUONPREPDATA_STGCPADAUXCONTAINER_H
#define XAODMUONPREPDATA_STGCPADAUXCONTAINER_H

#include "xAODMuonPrepData/sTgcPadHitFwd.h"
#include "xAODMuonPrepData/versions/sTgcPadAuxContainer_v1.h"


// Set up a CLID for the class:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::sTgcPadAuxContainer , 1246485600 , 1 )
#endif  // XAODMUONPREPDATA_STGCSTRIPAUXCONTAINER_H