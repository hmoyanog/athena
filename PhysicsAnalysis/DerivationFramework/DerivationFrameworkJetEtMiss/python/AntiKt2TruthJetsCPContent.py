# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

AntiKt2TruthJetsCPContent = [
"AntiKt2TruthJets",
"AntiKt2TruthJetsAux.pt.eta.phi.m.PartonTruthLabelID.HadronConeExclExtendedTruthLabelID.HadronConeExclTruthLabelID.TrueFlavor"
]

