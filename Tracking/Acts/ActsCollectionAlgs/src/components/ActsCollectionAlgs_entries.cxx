/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "src/TrackContainerReader.h"

// Algs
DECLARE_COMPONENT( ActsTrk::TrackContainerReader )
